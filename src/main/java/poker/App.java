package poker;

import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 *	1) Generate Players with 5 cards;
 *  2) Sort by Rank
 *  3) Validate which will rank again if necessary.
 */
public class App 
{
    public static void main( String[] args )
    {
         List<Player> players = Arrays.asList(
             new Player("Player1"), 
             new Player("Player2"), 
             new Player("Player4"), 
             new Player("Player5"), 
             new Player("Player3"));
         players.forEach(p -> {
             p.addCard(CardGenerator.generateCard());
             p.addCard(CardGenerator.generateCard());
             p.addCard(CardGenerator.generateCard());
             p.addCard(CardGenerator.generateCard());
             p.addCard(CardGenerator.generateCard());
             p.calculateRank();
         });

        players.sort((p1, p2) -> p1.getRank().ordinal() - p2.getRank().ordinal());
        ValidateRank validate = new ValidateRank();
        validate.validate(players);
        System.out.println("Players ranked ordered from highest to lowest");
        players.forEach(p -> System.out.println(p));

    }
        static void ranking(List<Player> players) {
        	
        }
}
