package poker;

import java.util.HashMap;
import java.util.Map;

class Card {
    private String suit;
    private int value;
    private final static Map<String, Integer> cardValues;
    static private String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

    static {
        cardValues = new HashMap<>();
        cardValues.put("2", 2);
        cardValues.put("3", 3);
        cardValues.put("4", 4);
        cardValues.put("5", 5);
        cardValues.put("6", 6);
        cardValues.put("7", 7);
        cardValues.put("8", 8);
        cardValues.put("9", 9);
        cardValues.put("10", 10);
        cardValues.put("J", 11);
        cardValues.put("Q", 12);
        cardValues.put("K", 13);
        cardValues.put("A", 14);
    }

    Card(String suit, String strValue) {
        this.suit = suit;
        this.value = cardValues.get(strValue);
    }
    public String getSuit() {
        return suit;
    }
    
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return ranks[value-2] + ":" + suit;
    }
}