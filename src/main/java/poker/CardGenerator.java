package poker;

import java.util.Random;

class CardGenerator {
    static private int[][] deck = new int[4][13];
    static private String[] suits = {"C", "D", "H", "S"};
    static private String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    static private Random rand = new Random();

    static Card generateCard() {
      int suit = rand.nextInt(3);
      int rank = rand.nextInt(13);
      while (deck[suit][rank] == 1) {
        suit = rand.nextInt(3);
        rank = rand.nextInt(13);
      }
        deck[suit][rank]++;
        return new Card(suits[suit], ranks[rank]);
    }
    
}
