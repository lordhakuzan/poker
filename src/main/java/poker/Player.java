package poker;

import java.util.ArrayList;
import java.util.List;

/**
 * Player
 */
class Player {
    private final static int SIZE_HAND = 5;
    private String name;
    // private Card[] cards = new Card[SIZE_HAND];
    private List<Card> cards = new ArrayList<>();
    private boolean sameSuit = true;
    private int[] history = new int[5];
    static final String HAND_VALUES[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    private Rank rank;

    List<Card> getCards() {
        return cards;
    }
    Player(String name) {
        this.name = name;
    }
    void addCard(Card card) {
        cards.add(card);
    }


    void order() {
        cards.sort((c1, c2) -> c1.getValue() - c2.getValue());
    }

    void hasSameSuit() {
        String suit = cards.get(0).getSuit();
        for (int i = 1; i < SIZE_HAND; i++)
           if (!suit.equals(cards.get(i).getSuit())) {
               sameSuit = false;
               break;
           }
    }

    void getSimilars() {
        int similar = 1;
        int newValue;
        int value = cards.get(0).getValue();
        // System.out.println(cards);
        for (int i = 1; i < SIZE_HAND; i++) {
            newValue = cards.get(i).getValue();
            if (value != newValue) {
                history[similar]++;
                similar = 1;
                value = newValue;
                continue;
            }
            similar++;
        }
        history[similar]++;
        // System.out.println(Arrays.toString(history));
    }

    void calculateRank() {
        order();
        getSimilars();
        hasSameSuit();
        if (isStright())
            if (sameSuit)
                this.rank = Rank.STRAIGHT_FLUSH;
            else
                this.rank = Rank.STRAIGHT;
        else if (history[4] == 1)
            this.rank = Rank.FOUR_OF_A_KIND;
        else if (history[3] == 1 && history[2] == 1)
            this.rank = Rank.FULL_HOUSE;
        else if (sameSuit)
            this.rank = Rank.FLUSH;
        else if (history[3] == 1)
            this.rank = Rank.THREE_OF_A_KIND;
        else if (history[2] == 2)
            this.rank = Rank.TWO_PAIRS;
        else if (history[2] == 1)
            this.rank = Rank.PAIR;
        else 
            this.rank = Rank.HIGH_CARD;
            
    }

    // boolean isStright() {
    //    String init = cards.get(0).getValue(); 
    //    int index = 0;
    //    while (!init.equals(HAND_VALUES[index]))
    //     index++;
    //    for (int i = 0; i < SIZE_HAND; i++)
    //     if (!cards.get(i).getValue().equals(HAND_VALUES[index++]))
    //         return false;
    //     return true;

       
    // }

    boolean isStright() {
       int init = cards.get(0).getValue(); 
       for(Card c: cards) 
            if (c.getValue() != init++) {
                return false;
            }
        return true;
    }

    @Override
    public String toString() {
        return name + "[Cards: " + cards + ", Rank: " + rank.getName() + "]";
    }
	public Rank getRank() {
		return rank;
	}
}