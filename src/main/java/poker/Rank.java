package poker;

public enum Rank {
    STRAIGHT_FLUSH("Straight Flush"),
    FOUR_OF_A_KIND("Four of a Kind"),
    FULL_HOUSE("Full House"),
    FLUSH("Flush"),
    STRAIGHT("Straight"),
    THREE_OF_A_KIND("Three of a kind"),
    TWO_PAIRS("Two Pairs"),
    PAIR("Pair"),
    HIGH_CARD("High Card");

    private final String name;

    private Rank(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
