package poker;

import java.util.ArrayList;
import java.util.List;

public class ValidateRank {
    void validate(List<Player> players) {
        for(int i = 0; i < players.size(); i++) {
                switch (players.get(i).getRank()) {

                    case FOUR_OF_A_KIND:
                        validateRank(players, i, Rank.FOUR_OF_A_KIND, 4);
                        break;
                    case FULL_HOUSE:
                    	validateRank(players, i , Rank.FULL_HOUSE, 3);
                        break;
                    case THREE_OF_A_KIND:
                    	validateRank(players, i, Rank.THREE_OF_A_KIND, 3);
                    case TWO_PAIRS:
                    	validatePairs(players, i, Rank.TWO_PAIRS);
                    	break;
                    case PAIR:
                    	validatePair(players, i, Rank.PAIR);
                    	break;
                    default:
                        validateHighest(players, i);
                }
        }
    }

private void validatePair(List<Player> players, int i, Rank rank) {
    	
    	while(i < players.size()) {
    		Player current = players.get(i);
        	if (current.getRank() != rank)
        		break;
    		int currentIndexPair = getIndexHighestPair(current.getCards(), 4);
    		int currentValue = current.getCards().get(currentIndexPair).getValue();
    		int max_j = i;
    		for(int j = i + 1; j < players.size(); j++) {
                Player next = players.get(j);
                if (next.getRank() != rank)
                	break;
                int nextIndexPair = getIndexHighestPair(next.getCards(), 4);
                int nextValue = current.getCards().get(nextIndexPair).getValue();
                if (currentValue < nextValue) {
                	max_j = j;
                	currentValue = nextValue;
                }
                else if (currentValue == nextValue) {

                	List<Card> currentNonPair = getNonPairs(current.getCards());
                	List<Card> nextNonPair = getNonPairs(next.getCards());
       			 	
       			 	int currentHighest = getHighestCard(currentNonPair, 1);
       			 	int nextHighest = getHighestCard(nextNonPair, 1);
       				 if (currentHighest < nextHighest) {
       					 max_j = j;
       					 break;
       				 }
       			 }
                
    		}
    		if (max_j != i)
    			swapPlayers(players, i, max_j);
    		i++;
    	}
	}

	private void validatePairs(List<Player> players, int i, Rank rank) {
    	
    	while(i < players.size()) {
    		Player current = players.get(i);
        	if (current.getRank() != rank)
        		break;
    		int currentIndexPair = getIndexHighestPair(current.getCards(), 4);
    		int currentValue = current.getCards().get(currentIndexPair).getValue();
    		int max_j = i;
    		for(int j = i + 1; j < players.size(); j++) {
                Player next = players.get(j);
                if (next.getRank() != rank)
                	break;
                int nextIndexPair = getIndexHighestPair(next.getCards(), 4);
                int nextValue = current.getCards().get(nextIndexPair).getValue();
                if (currentValue < nextValue)
                	max_j = j;
                else if (currentValue == nextValue) {

                		 currentIndexPair = getIndexHighestPair(current.getCards(), currentIndexPair);
                		 currentValue = current.getCards().get(currentIndexPair).getValue();
                		 nextIndexPair = getIndexHighestPair(next.getCards(), nextIndexPair);
                		 nextValue = current.getCards().get(currentIndexPair).getValue();
                		 if (currentValue < nextValue)
                         	max_j = j;
                		 else if (currentValue == nextValue) {
                			 List<Card> currentNonPair = getNonPairs(current.getCards());
                			 List<Card> nextNonPair = getNonPairs(next.getCards());
                			 for (int index = 0; index < currentNonPair.size(); index++) {
                				 if (currentNonPair.get(index).getValue() < nextNonPair.get(index).getValue()) {
                					 max_j = j;
                					 break;
                				 }
                			 }
                		 }

                }
                	
                
    		}
    		if (max_j != i)
    			swapPlayers(players, i, max_j);
    		i++;
    	}
	}

    private List<Card> getNonPairs(List<Card> cards) {
    	List<Card> result = new ArrayList<>();
    	for(int i = 0; i < cards.size(); i++) { 
    		for (int j = 0; j < cards.size(); j++) {
    			if ((j + 1) == cards.size())
    				result.add(cards.get(i));
    			if (i == j)
    				continue;
    			if (cards.get(i).getValue() == cards.get(j).getValue())
    				break;
    			
    		}
    	}
    	return result;
	}

	private int getIndexHighestPair(List<Card> cards, int index) {
    	int value = cards.get(index).getValue();
    	int i = index - 1;
    	for (; i >= 0; i--) {
    		if(value != cards.get(i).getValue())
    			value = cards.get(i).getValue();
    		else
    			return i;
    	}
    	return i;
    }
	
	private void validateHighest(List<Player> players, int i) {
        Player current = players.get(i);
        int currentHighest;
        int last = 1;
        while(i < players.size()) {
            currentHighest = getHighestCard(current.getCards(), last);
            int max_j = i;
            for(int j = i; j < players.size(); j++) {
                Player next = players.get(j);
                int nextHighest = getHighestCard(next.getCards(), last);
                if (currentHighest < nextHighest)
                    max_j = j;
                if (currentHighest == nextHighest && last < 5 )
                    currentHighest = getHighestCard(current.getCards(), ++last);
            }
            if (max_j != i)
                swapPlayers(players, i, max_j);
            i++;
        }
    }

    void validateFH(List<Player> players, int i, Rank rank) {
        
        while (i < players.size()) {
        	Player p = players.get(i);
        	if (p.getRank() != rank)
        		break;
            int index_max = i;
            int value = getCardValueByofaKind(p.getCards(), 3);
            for(int j = i+1; j < players.size(); j++) {
                Player next = players.get(j);
                if (next.getRank() != rank)
                    break;
                if (value < getCardValueByofaKind(players.get(j).getCards(), 3))
                    index_max = j;
            }
            if (index_max != i) {
                swapPlayers(players, i, index_max);
            }
            i++;
        }
    }

    private int getCardValueByofaKind(List<Card> cards, int kind) {
        Card card = cards.get(0);
        int iter = 0;
        for (int i = 0; i < cards.size(); i++) {
        	if (iter == kind)
        		return card.getValue();
        	if (card.getValue() == cards.get(i).getValue())
        		iter++;
        	else {
        		iter = 0;
        		card = cards.get(i);
        	}
        }
        return card.getValue();
    }

    void validateRank(List<Player> players, int i, Rank rank, int numberOfCards) {
        
        while (i < players.size()) {
        	Player p = players.get(i);
        	if (p.getRank() != rank)
        		break;
            int currentHighestCard = getCardValueByofaKind(p.getCards(), numberOfCards);
            int max_j = i;
            for(int j = i+1; j < players.size(); j++) {
                Player next = players.get(j);
                if (next.getRank() != rank)
                    break;
                if (currentHighestCard < getCardValueByofaKind(next.getCards(), numberOfCards)){
                    max_j = j;
                }
            }
            if (max_j != i)
                swapPlayers(players, i, max_j);
            i++;
        }
    }

    private int getHighestCard(List<Card> cards, int last) {
        return cards.get(cards.size() - last).getValue();
    }

    private void swapPlayers(List<Player> players, int i, int j) {
        Player temp = players.get(i);
        players.set(i, players.get(j));
        players.set(j, temp);
    }
}
