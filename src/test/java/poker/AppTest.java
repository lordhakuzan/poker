package poker;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    void checkFullHouse() {
        Player p = new Player("One");
        p.addCard(new Card("C", "2"));
        p.addCard(new Card("D", "2"));
        p.addCard(new Card("H", "2"));
        p.addCard(new Card("C", "3"));
        p.addCard(new Card("H", "3"));
        p.calculateRank();
        assertEquals(Rank.FULL_HOUSE, p.getRank());
    }
}
